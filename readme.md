## Install a JWT library

composer require lcobucci/jwt "3.1" (PHP5.3<)
composer require lcobucci/jwt "4.0" (PHP7)


## Remarks
I had to change "do" method, since it is a reserved method, and in previous versions of PHP gives problems.


