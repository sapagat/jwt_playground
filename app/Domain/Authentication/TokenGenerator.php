<?php
namespace App\Domain\Authentication;

interface TokenGenerator
{
  public function make(User $user);
}