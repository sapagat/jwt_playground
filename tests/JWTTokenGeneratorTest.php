<?php 

use App\Domain\Authentication\Credentials;
use App\Domain\Authentication\Infrastructure\JWTTokenGenerator;
use App\Domain\Authentication\User;

class JWTTokenGeneratorTest extends TestCase
{
  /** @test */
  public function it_generates_a_token()
  {
    $aUser = $this->obtainUser();
    $token = (new JWTTokenGenerator)->make($aUser);
    $this->assertJWTToken($token);
  }

  private function obtainUser()
  {
    $username = 'paco';
    $password = '132pass';
    $credentials = new Credentials($username,$password);
    return new User($credentials);
  }

  private function assertJWTToken($token)
  {
    $this->assertNotNull($token,'Token is null');
    $this->assertInternalType('string',$token,'Token not a string');
  }
}